diff -Naur linux-4.9.24/init/main.c linux-4.9.24_modified/init/main.c
--- linux-4.9.24/init/main.c	2017-04-21 12:01:39.000000000 +0430
+++ linux-4.9.24_modified/init/main.c	2018-01-23 01:19:48.384616258 +0330
@@ -937,8 +937,15 @@
 
 static int __ref kernel_init(void *unused)
 {
+	//the parameter required for setting the scheduler
+	struct sched_param param = { .sched_priority = MAX_RT_PRIO - 1 };
+
+
 	int ret;
 
+	//setting the scheduling algorithm to FCFS
+	sched_setscheduler_nocheck(current, SCHED_FIFO, &param);
+
 	kernel_init_freeable();
 	/* need to finish all async __init code before freeing the memory */
 	async_synchronize_full();
