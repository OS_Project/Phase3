#include <stdio.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>

int takeInput(char* input);
int processCommand(char *input);
int parse(char *command, char **parsed, char **pipeParsed);

bool verify(char *inputCommand);

int forkAndRunSimpleCommand(char **parsed);

int forkAndRunPipeCommand(char **beforePipeParsed, char **parsed);

bool builtInCommandRunner(char **parsed);

void parseSpace(char *command, char **parsed);

void printCurrentDirectory();

void printUserName();

const int MAX_SIZE = 50;

int main() {

    while(true) {
	printUserName();
	printCurrentDirectory();
        char input[1000];
        takeInput(input);
//        if (strcmp(input, "help") == 0)
//            printf("Hooray\n");
//        else
//            printf("NONONONO\n");
//        printf("%s", input);
        if (!verify(input))
            continue;
//        printf("%s", input);
        int status = processCommand(input);
        if (status != 0)
            printf("an error occurred when running command\n");
    }

    return 0;
}

int processCommand(char *input) {
    char* afterPipeParsed[MAX_SIZE], *parsed[MAX_SIZE];
//    printf("%s", input);
    int pipeStatus = parse(input, parsed, afterPipeParsed);
    if (pipeStatus == -1)
        exit(1);

//    printf("%s", parsed[0]);
//    printf("%s", afterPipeParsed[0]);


    if (builtInCommandRunner(parsed))
        return 0;
    if (pipeStatus == 0)
        return forkAndRunSimpleCommand(parsed);
    if (pipeStatus == 1)
        return forkAndRunPipeCommand(parsed, afterPipeParsed);
}

bool builtInCommandRunner(char **parsed) {
    int num = 3;
    char* ownCommands[num];
    ownCommands[0] = "cd";
    ownCommands[1] = "help";
    ownCommands[2] = "exit";

    int cmdNum = -1;
    for(int i = 0; i < num; ++i)
        if (strcmp(parsed[0], ownCommands[i]) == 0) {
            cmdNum = i;
            break;
        }

    switch (cmdNum) {
        case 0:
            chdir(parsed[1]);
            break;
        case 1:
            printf("cd \'absolute/relative address\' : to change directory\n"
                           "exit : to exit shell\n"
                           "other commands to run programs\n");
            break;
        case 2:
            exit(0);
        default:
            return 0;
    }
}

int forkAndRunPipeCommand(char **beforePipeParsed, char **afterPipeParsed) {
    int fd[2];
    if (pipe(fd) < 0) {
        printf("could not build pipe");
        return 1;
    }

    pid_t first = fork();
    if (first < 0) {
        printf("First child could not be forked\n");
        return 1;
    }
    else if (first == 0) {
        close(fd[0]);
        dup2(fd[1], STDOUT_FILENO);
        close(fd[1]);
        if (execvp(beforePipeParsed[0], beforePipeParsed) < 0) {
            printf("First child did not run correctly\n");
            return 1;
        }
        return 0;
    }
    else {
        wait(NULL);
        pid_t second = fork();
        if (second < 0) {
            printf("First child could not be forked\n");
            return 1;
        }
        else if (second == 0) {
            close(fd[1]);
            dup2(fd[0], STDIN_FILENO);
            close(fd[0]);
            if (execvp(afterPipeParsed[0], afterPipeParsed) < 0) {
                printf("Second child did not run correctly\n");
                return 1;
            }
            return 0;
        }
        else {
            wait(NULL);
        }
    }
    return 0;
}

int forkAndRunSimpleCommand(char **parsed) {
    pid_t pid = fork();
    if (pid < 0) {
        printf("Child could not be forked\n");
        return 1;
    }
    else if (pid == 0) {
        if (execvp(parsed[0], parsed) < 0) {
            printf("Child did not run correctly\n");
            return 1;
        }
        return 0;
    }
    else
        wait(NULL);
    return 0;
}

bool verify(char *inputCommand) {
    if (inputCommand == NULL || strlen(inputCommand) == 0)
        return 0;
    bool hasNonSpace = 0;
    for(int i = 0; i < strlen(inputCommand); ++i)
        if (inputCommand[i] != ' ') {
            hasNonSpace = 1;
            break;
        }
    if (!hasNonSpace)
        return 0;

    return 1;
}

int parse(char *command, char **parsed, char **pipeParsed) {
    char* pipedPart[2];
    int pipeStatus = -1;
    for (int i = 0; i < 2; ++i) {
        pipedPart[i] = strsep(&command, "|");
    }

    if (pipedPart[0] != NULL) {
        parseSpace(pipedPart[0], parsed);
        pipeStatus = 0;
    }
    if (pipedPart[1] != NULL) {
        parseSpace(pipedPart[1], pipeParsed);
        pipeStatus = 1;
    }
    return pipeStatus;
}

void parseSpace(char *command, char **parsed) {
    for (int i = 0; i < MAX_SIZE; ++i) {
        parsed[i] = strsep(&command, " ");
        if (parsed[i] == NULL)
            break;
        if (parsed[i] == " ")
            --i;
    }
}

int takeInput(char *input) {
    char *tmp;
    tmp = readline("$ ");
    add_history(tmp);
    strcpy(input, tmp);
//    printf("%s", strcat(input, "\n"));
    return 1;
}

void printCurrentDirectory(){
    char cwd[1024];
    getcwd(cwd, sizeof(cwd));
    printf("%s", cwd);
}

void printUserName(){
    char* username = getenv("USER");
    printf("\n%s: ", username);
}
